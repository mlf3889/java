/*
 * Marc Funston
 * 
 * 
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Marc
 */
public class Color {

    private static String nameOfFile;
    private static int numberOfColors;
    private static Scanner inputFromFile;

    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static int count = 0;

    private static Stack stack;
    private static Integer[] clipboardArray;
    private static Integer[][] arrayOfNodes;
    private static Integer dimensionOfArray;
    private static int[] nodeColor;
    private static Integer[][] graph;

    public static void main(String[] args) throws FileNotFoundException {

        stack = new Stack<>();
        nameOfFile = args[0];
        java.io.File fileName = new java.io.File(nameOfFile);
        numberOfColors = Integer.parseInt(args[1]);
        inputFromFile = new Scanner(fileName);
        dimensionOfArray = Integer.parseInt(inputFromFile.next());
        clipboardArray = new Integer[100];
        arrayOfNodes = new Integer[dimensionOfArray][dimensionOfArray];

        inputFromFile();
        fillTwoDimensionArray();

        // pop node from the stack
        for (int increment = ZERO; increment < dimensionOfArray; increment++) {
            pushArrayToStack();
            //printOutArray();
            removeElement();
        }

        // put the node back to the graph
        for (int increment = 0; increment < dimensionOfArray; increment++) {
            arrayOfNodes = (Integer[][]) stack.pop();
            //printOutArray();

            // Check assigned colors
            // Use an existing color if nodes are not connected; otherwise use a new color
            // If the number of used colors is larger than K+1 
            // Output a message â€œThe graph is not K colorableâ€ 
            // Terminate the program; otherwise repeat until empty
            colorTheGraph();
        }
    }

    public static void colorTheGraph() {
        nodeColor = new int[dimensionOfArray];
        graph = arrayOfNodes;
        try {
            solveColors(0);
            clearTheScreen();
            System.out.println("The graph is not " + numberOfColors + " colorable");
        } catch (Exception e) {
            displayColorResults();
        }
    }

    public static void solveColors(int node) throws Exception {
        if (node == dimensionOfArray) {
            throw new Exception("Solution found");
        }
        solve(node);
    }

    public static void solve(int node) throws Exception {
        for (int count = 1; count <= numberOfColors; count++) {
            if (canItBeSolved(node, count)) {
                nodeColor[node] = count;
                solve(node + ONE);
                nodeColor[node] = ZERO;
            }
        }
    }

    public static boolean canItBeSolved(int node, int count) throws IOException {
        for (int increment = 0; increment < dimensionOfArray; increment++) {
            if (graph[node][increment] == ONE && count == nodeColor[increment]) {
                return false;
            }
        }
        return true;
    }

    public static void displayColorResults() {
        for (int increment = 0; increment < dimensionOfArray; increment++) {
            System.out.print("Node = " + increment + " Color = C" + nodeColor[increment] + "\n");
        }
        System.out.println();
    }

    private static void pushArrayToStack() {
        Integer[][] newArray = new Integer[dimensionOfArray][dimensionOfArray];
        for (int row = ZERO; row < dimensionOfArray; row++) {
            System.arraycopy(arrayOfNodes[row], ZERO, newArray[row], ZERO, dimensionOfArray - ZERO);
        }
        stack.push(newArray);
    }

    private static int removeElement() {
        int row = findSmallestSet(arrayOfNodes, dimensionOfArray);
        for (int column = ONE; column < dimensionOfArray; column++) {
            if (arrayOfNodes[row][column] == ONE) {
                arrayOfNodes[column][row] = ZERO;
                arrayOfNodes[row][column] = ZERO;
            }
        }
        return ONE;
    }

    private static int findSmallestSet(Integer[][] arrayOfNodes, Integer dimensionOfArray) {
        int[] testCase = rowTotals();
        int smallestRow = ZERO;
        int smallestNumberOfNodes = testCase[ZERO];
        for (int increment = ONE; increment < dimensionOfArray; increment++) {
            if (testCase[increment] < smallestNumberOfNodes) {
                smallestRow = increment;
                smallestNumberOfNodes = testCase[increment];
            }
        }
        return smallestRow;
    }

    private static int[] rowTotals() {
        int[] rowTotals = new int[dimensionOfArray];
        for (int row = ZERO; row < dimensionOfArray; row++) {
            for (int column = ZERO; column < dimensionOfArray; column++) {
                rowTotals[row] += arrayOfNodes[row][column];
            }
            if (rowTotals[row] == ZERO) {
                rowTotals[row] = 100; // prevents zero being smallest number of elements every time
            }
        }
        return rowTotals;
    }

    private static void fillTwoDimensionArray() {
        int increment = 0;
        for (int row = ZERO; row < dimensionOfArray; row++) {
            for (int column = ZERO; column < dimensionOfArray; column++) {
                arrayOfNodes[row][column] = clipboardArray[increment];
                increment++;
            }
        }
    }

    private static void printOutArray() {
        for (int row = ZERO; row < dimensionOfArray; row++) {
            for (int column = ZERO; column < dimensionOfArray; column++) {
                System.out.print(arrayOfNodes[row][column] + " ");

            }
            System.out.println("");
        }
        System.out.println("");
    }

    private static void clearTheScreen() {
        for (int increment = 0; increment < 100; increment++) {
            System.out.println("\n");
        }
    }

    private static void inputFromFile() {
        do {
            clipboardArray[count] = Integer.parseInt(inputFromFile.next());
            count++;
        } while (inputFromFile.hasNext());
    }
}
