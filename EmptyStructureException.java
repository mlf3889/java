/**
 *
 * @author Marc
 */
public class EmptyStructureException extends RuntimeException {

    public EmptyStructureException() {
        super();
        System.out.println("I'm sorry that stack is empty, press \"Enter\"");
    }
}