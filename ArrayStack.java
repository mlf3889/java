/**
 *
 * @author Marc
 */
public class ArrayStack<E> implements Stack<E> {

    // Array of items in the stack.
    private E[] data;

    // Number of items currently in the stack.
    private int size;
    private boolean possible = true;

    // The stack is initially empty.
    public ArrayStack() {
        data = (E[]) (new Object[1]); // This causes a compiler warning
        size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public E pop() { //
        if (isEmpty()) {
            throw new EmptyStructureException();
        }
        size--;
        return (E) data[size];
    }

    public E peek() { //
        if (isEmpty()) {
            throw new EmptyStructureException();
        }
        return (E) data[size - 1];
    }

    // Return true if data is full.
    protected boolean isFull() {
        return size == data.length;
    }


    public String toString() {
        String result = "";

        if (isEmpty()) {
            result += "Stack is Empty\n";
            return result;
        }
        result += "Size = "+getSize()+"\n";
        result += "Index: ";
    
        for (int i=0; i<size; i++) {
            result += i + " ";
        }
        result += "\n";
        result += "Data: ";
    
        for (int i=0; i<size; i++) {
            result += data[i] + " ";
        }
        result += "\n";
        return result;
    }

    public void push(E target) {
        if (isFull()) {
            stretch();
        }
        data[size] = target;
        size++;
    }

    public int getSize() {
        return size;
    }


    // Double the length of data.
    protected void stretch() {
        E[] newData = (E[]) (new Object[data.length * 2]);  // warning
        for (int i = 0; i < data.length; i++) {
            newData[i] = data[i];
        }
        data = newData;
    }
}
